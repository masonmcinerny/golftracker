using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using golftracker.Data;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;

namespace golftracker.Controllers
{
    [Authorize]
    [Route("api/scores")]
    [ApiController]
    public class ScoreController : ControllerBase
    {
        private readonly ApiDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public ScoreController(ApiDbContext context,  UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: api/scores
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Score>>> GetScores()
        {
            var userName = User.Identity.Name;
            var userScores = await _context.Scores
                .Where(score => score.UserName == userName)
                .ToListAsync();

            return userScores;
        }

        // GET: api/scores/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<Score>> GetScore(int id)
        {
            var score = await _context.Scores.FindAsync(id);

            if (score == null)
            {
                return NotFound();
            }

            return score;
        }

        // POST: api/scores
[HttpPost]
public async Task<ActionResult<Score>> PostScore(Score score)
{
    // Ensure the associated GolfCourse exists
    var golfCourse = await _context.GolfCourses.FindAsync(score.GolfCourseId);
    if (golfCourse == null)
    {
        return BadRequest("The specified GolfCourse does not exist.");
    }

    // Get the user's email address from claims
    var userEmail = User.FindFirstValue(ClaimTypes.Email);

    // Set the UserName property to the user's email
    score.UserName = userEmail;

    _context.Scores.Add(score);
    await _context.SaveChangesAsync();

    return CreatedAtAction(nameof(GetScore), new { id = score.Id }, score);
}

        // PUT: api/scores/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> PutScore(int id, Score score)
        {
            if (id != score.Id)
            {
                return BadRequest("Incorrect score ID");
            }

            _context.Entry(score).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE: api/scores/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteScore(int id)
        {
            var score = await _context.Scores.FindAsync(id);
            if (score == null)
            {
                return NotFound();
            }

            _context.Scores.Remove(score);
            await _context.SaveChangesAsync();

            return NoContent();
        }

// GET: api/scores/{userName}/{courseId}
[HttpGet("{userName}/{courseId}")]
public async Task<ActionResult<IEnumerable<Score>>> GetScoresByCourse(string userName, int courseId)
{
    var userScoresByCourse = await _context.Scores
        .Where(score => score.UserName == userName && score.GolfCourseId == courseId)
        .ToListAsync();

    return userScoresByCourse;
}

    [HttpGet("user/{userName}")]
    public async Task<ActionResult<IEnumerable<Score>>> GetScoresByUser(string userName)
    {
        // Use _userManager to retrieve user information
        var user = await _userManager.FindByNameAsync(userName);
        if (user == null)
        {
            return NotFound("User not found.");
        }

        var userScores = await _context.Scores
            .Where(score => score.UserName == userName)
            .ToListAsync();

        return userScores;
    }

    }
}
