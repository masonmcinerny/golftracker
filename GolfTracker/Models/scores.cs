using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
#nullable disable
public class Score
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    public string UserName { get; set; }

    public int GolfCourseId { get; set; }

    [DataType(DataType.Date)]
    public DateTime EntryDate { get; set; }

    public string Tees { get; set; } = "Blues";
    public int StartingPosition { get; set; } = 1;
    public int Holes { get; set; }
    public int Putts { get; set; }
    public int TotalScore { get; set; }
    public string Notes { get; set; }
}
