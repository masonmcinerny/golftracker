using System.ComponentModel.DataAnnotations;


namespace golftracker.Models.DTOS;
public class AuthResult
{
    public string Token { get; set; } = string.Empty;
    public bool Result { get; set; }
    public List<string>? Errors { get; set; }
}
