using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class GolfCourse
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    public string CourseName { get; set; } = null!;
    public string Location { get; set; } = null!;
    public int Par { get; set; }
}
