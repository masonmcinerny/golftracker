﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace GolfTracker.Migrations
{
    /// <inheritdoc />
    public partial class CoursesAndScores : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GolfCourses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CourseName = table.Column<string>(type: "TEXT", nullable: false),
                    Location = table.Column<string>(type: "TEXT", nullable: false),
                    Par = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GolfCourses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Scores",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<string>(type: "TEXT", nullable: false),
                    GolfCourseId = table.Column<int>(type: "INTEGER", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Tees = table.Column<string>(type: "TEXT", nullable: false),
                    StartingPosition = table.Column<int>(type: "INTEGER", nullable: false),
                    Holes = table.Column<int>(type: "INTEGER", nullable: false),
                    Putts = table.Column<int>(type: "INTEGER", nullable: false),
                    TotalScore = table.Column<int>(type: "INTEGER", nullable: false),
                    Notes = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Scores", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Scores_GolfCourses_GolfCourseId",
                        column: x => x.GolfCourseId,
                        principalTable: "GolfCourses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "GolfCourses",
                columns: new[] { "Id", "CourseName", "Location", "Par" },
                values: new object[,]
                {
                    { 1, "Coyote Creek", "Redmond, Wa", 72 },
                    { 2, "Eagles Talon", "Redmond, Wa", 71 },
                    { 3, "Maplewood Golf Course", "Renton, Wa", 71 },
                    { 4, "Fosters Golf Links", "Tukwila, Wa", 68 },
                    { 5, "Auburn Golf Course", "Auburn, Wa", 71 },
                    { 6, "West Seattle Golf Course", "Seattle, Wa", 72 },
                    { 7, "Snoqualmie Falls", "Fall City, Wa", 71 },
                    { 8, "Allenmore Golf Course", "Tacoma, Wa", 71 },
                    { 9, "Redmond Ridge", "Redmond, Wa", 72 },
                    { 10, "Mount Si Golf Course", "Snoqualmie, WA", 72 },
                    { 11, "Riverbend Golf Course", "Kent, Wa", 72 },
                    { 12, "Bellevue Golf Course", "Bellevue, Wa", 71 },
                    { 13, "Apple Tree Golf Course", "Yakima, Wa", 72 },
                    { 14, "Druid Glenn Golf Club", "Covington, Wa", 72 },
                    { 15, "The Links at Moses Pointe", "Moses Lake, Wa", 72 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Scores_GolfCourseId",
                table: "Scores",
                column: "GolfCourseId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Scores");

            migrationBuilder.DropTable(
                name: "GolfCourses");
        }
    }
}
