﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GolfTracker.Migrations
{
    /// <inheritdoc />
    public partial class UpdatedScoreModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Scores_GolfCourses_GolfCourseId",
                table: "Scores");

            migrationBuilder.DropIndex(
                name: "IX_Scores_GolfCourseId",
                table: "Scores");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Scores",
                newName: "UserName");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserName",
                table: "Scores",
                newName: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Scores_GolfCourseId",
                table: "Scores",
                column: "GolfCourseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Scores_GolfCourses_GolfCourseId",
                table: "Scores",
                column: "GolfCourseId",
                principalTable: "GolfCourses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
