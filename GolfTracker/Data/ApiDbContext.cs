using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;



namespace golftracker.Data;

public class ApiDbContext : IdentityDbContext
{
    public ApiDbContext(DbContextOptions<ApiDbContext> options)
    : base(options)
    {

    }
    public DbSet<GolfCourse> GolfCourses { get; set; }
 public DbSet<Score> Scores { get; set; }
protected override void OnModelCreating(ModelBuilder modelBuilder)
{
    base.OnModelCreating(modelBuilder);

    var golfCourses = new List<GolfCourse>
    {
        new GolfCourse { Id = 1, CourseName = "Coyote Creek", Location = "Redmond, Wa", Par = 72 },
        new GolfCourse { Id = 2, CourseName = "Eagles Talon", Location = "Redmond, Wa", Par = 71 },
        new GolfCourse { Id = 3, CourseName = "Maplewood Golf Course", Location = "Renton, Wa", Par = 71 },
        new GolfCourse { Id = 4, CourseName = "Fosters Golf Links", Location = "Tukwila, Wa", Par = 68 },
        new GolfCourse { Id = 5, CourseName = "Auburn Golf Course", Location = "Auburn, Wa", Par = 71 },
        new GolfCourse { Id = 6, CourseName = "West Seattle Golf Course", Location = "Seattle, Wa", Par = 72 },
        new GolfCourse { Id = 7, CourseName = "Snoqualmie Falls", Location = "Fall City, Wa", Par = 71 },
        new GolfCourse { Id = 8, CourseName = "Allenmore Golf Course", Location = "Tacoma, Wa", Par = 71 },
        new GolfCourse { Id = 9, CourseName = "Redmond Ridge", Location = "Redmond, Wa", Par = 72 },
        new GolfCourse { Id = 10, CourseName = "Mount Si Golf Course", Location = "Snoqualmie, WA", Par = 72 },
        new GolfCourse { Id = 11, CourseName = "Riverbend Golf Course", Location = "Kent, Wa", Par = 72 },
        new GolfCourse { Id = 12, CourseName = "Bellevue Golf Course", Location = "Bellevue, Wa", Par = 71 },
        new GolfCourse { Id = 13, CourseName = "Apple Tree Golf Course", Location = "Yakima, Wa", Par = 72 },
        new GolfCourse { Id = 14, CourseName = "Druid Glenn Golf Club", Location = "Covington, Wa", Par = 72 },
        new GolfCourse { Id = 15, CourseName = "The Links at Moses Pointe", Location = "Moses Lake, Wa", Par = 72 }
    };

    modelBuilder.Entity<GolfCourse>().HasData(golfCourses);
}
}
