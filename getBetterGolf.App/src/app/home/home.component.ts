import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { ScoreService } from '../services/score.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  selectedCourseId: number | null = null;
  isLoggedIn = false;
  userScores: any[] = [];
  originalUserScores: any[] = [];
  courseIds: number[] = [];
  filteredScores: any[] = [];

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private scoreService: ScoreService
  ) {
    this.isLoggedIn = authService.isLoggedIn();
  }

  ngOnInit(): void {
    this.isLoggedIn = this.authService.isLoggedIn();
    if (this.isLoggedIn) {
      this.fetchUserScores();
    }
}



  mapCourseName(courseId: number): string {
    const courseMap: Record<number, string> = {
      1: 'Coyote Creek',
      2: 'Eagles Talon',
      3: 'Maplewood Golf Course',
      4: 'Fosters Golf Links',
      5: 'Auburn Golf Course',
      6: 'West Seattle Golf Course',
      7: 'Snoqualmie Falls',
      8: 'Allenmore Golf Course',
      9: 'Redmond Ridge',
      10: 'Mount Si Golf Course',
      11: 'Riverbend Golf Course',
      12: 'Bellevue Golf Course',
      13: 'Apple Tree Golf Course',
      14: 'Druid Glenn Golf Club',
      15: 'The Links at Moses Pointe',
    };
    const courseName = courseMap[courseId] || 'Unknown Course';
    return courseName;
  }

  extractCourseIds(): void {
    this.courseIds = [...new Set(this.userScores.map((score) => score.golfCourseId))];
  }
  applyCourseFilter(): void {
    if (!this.selectedCourseId) {
      this.filteredScores = [...this.userScores];
    } else {
      this.scoreService.getUserScoresByCourse(
        this.authService.getUserEmail()!,
        this.selectedCourseId
      ).subscribe(
        (filteredScores) => {
          this.filteredScores = filteredScores;
        },
        (error) => {
          console.error('Error filtering scores', error);
        }
      );
    }
  }

  fetchUserScores() {
    const userEmail = this.authService.getUserEmail()!;
    this.scoreService.getUserScoresByEmail(userEmail).subscribe(
      (scores) => {
        this.userScores = scores;
        this.originalUserScores = scores;


        this.courseIds = [...new Set(scores.map((score) => score.golfCourseId))];


        this.filteredScores = [...scores];
      },
      (error) => {
        console.error('Error loading user scores', error);
      }
    );
  }

  signOut() {
    this.authService.signOut();
    this.router.navigate(['/login']);
  }

  goToScorePost() {
    this.router.navigate(['/addscore']);
  }
  mapCourseLink(courseId: number): string {
    return this.mapCourseName(courseId);
  }
  goToCoursePage(courseId: number): void {
    this.router.navigate(['/course', courseId]);
}


}
