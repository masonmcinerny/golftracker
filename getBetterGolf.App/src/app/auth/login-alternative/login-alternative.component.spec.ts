import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginAlternativeComponent } from './login-alternative.component';

describe('LoginAlternativeComponent', () => {
  let component: LoginAlternativeComponent;
  let fixture: ComponentFixture<LoginAlternativeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginAlternativeComponent]
    });
    fixture = TestBed.createComponent(LoginAlternativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
