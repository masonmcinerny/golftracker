import { Component } from '@angular/core';
import { Login } from '../../models/login';
import { JwtAuth } from '../../models/jwtAuth';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-alternative',
  templateUrl: './login-alternative.component.html',
  styleUrls: ['./login-alternative.component.css']
})
export class LoginAlternativeComponent {
  loginDto = new Login();
  jwtDto = new JwtAuth();

  constructor(private authService: AuthenticationService, private router: Router){}

  login(loginDto: Login){
    this.authService.login(loginDto).subscribe((jwtDto) =>
    {
      localStorage.setItem('jwtToken', jwtDto.token)
      this.router.navigate(['/home']);
    });
  }
  navigateToRegister() {
    this.router.navigate(['/auth/register']);
  }
}
