import { Component } from '@angular/core';
import { Login } from '../../models/login';
import { JwtAuth } from '../../models/jwtAuth';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginDto = new Login();
  jwtDto = new JwtAuth();

  constructor(private authService: AuthenticationService, private router: Router){}

  login(loginDto: Login){
    this.authService.login(loginDto).subscribe((jwtDto) =>
    {
      localStorage.setItem('jwtToken', jwtDto.token)
      this.router.navigate(['/home']);
    });
  }
  navigateToLogin(){
    this.router.navigate(['auth/login-alternative'])
  }
  navigateToRegister() {
    this.router.navigate(['/auth/register']);
  }
}
