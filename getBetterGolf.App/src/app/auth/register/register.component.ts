import { Component } from '@angular/core';
import { JwtAuth } from '../../models/jwtAuth';
import { AuthenticationService } from '../../services/authentication.service';
import { Register } from '../../models/register';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  registerDto = new Register();

  constructor(private authService: AuthenticationService, private router: Router){}

  register(registerDto: Register){
    this.authService.register(registerDto).subscribe((response: JwtAuth) => {
      console.log('Registration Response:', response);

      if (response.result) {
        this.router.navigate(['/home']);

      } else {
        console.error('Registration failed:', response.error);
      }
    });
  }
}
