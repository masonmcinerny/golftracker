import { Component } from '@angular/core';
import { Login } from './models/login';
import { Register } from './models/register';
import { JwtAuth } from './models/jwtAuth';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'getBetterGolf.App';
  loginDto = new Login();
  registerDto = new Register();
  jwtDto = new JwtAuth();

  constructor(private authService: AuthenticationService, private router: Router){}

  register(registerDto: Register){
    this.authService.register(registerDto).subscribe((response: JwtAuth) => {
      console.log('Registration Response:', response);

      if (response.result) {
        console.log('Registration was successful');
        console.log('User Information:');
        console.log('Name:', registerDto.name);
        console.log('Email:', registerDto.email);
        // You can add more properties as needed
      } else {
        console.error('Registration failed:', response.error);
      }
    });
  }

  login(loginDto: Login){
    this.authService.login(loginDto).subscribe((jwtDto) =>
    {
      localStorage.setItem('jwtToken', jwtDto.token)
    });
  }

  weather(){
    this.authService.getWeather().subscribe((weatherdata: any) => {
      console.log(weatherdata);
    })
  }
  navigateToLogin() {
    this.router.navigate(['/auth/login']);
  }
  navigateToRegister(){
    this.router.navigate(['/auth/register']);
  }

}
