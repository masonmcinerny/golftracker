import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ScoreService } from '../services/score.service';

@Component({
  selector: 'app-score-post',
  templateUrl: './score-post.component.html',
  styleUrls: ['./score-post.component.css']
})
export class ScorePostComponent {
  score: any = {
    golfCourseId: null,
    entryDate: '',
    tees: '',
    startingPosition: null,
    holes: null,
    putts: null,
    totalScore: null,
    notes: '',
    UserName: '' // You can populate this with the user's email from authentication
  };
  courses: any[] = [
    { id: 1, name: 'Coyote Creek' },
    { id: 2, name: 'Eagles Talon' },
    { id: 3, name: "Maplewood Golf Course" },
    { id: 4, name: "Fosters Golf Links" },
    { id: 5, name: "Auburn Golf Course" },
    { id: 6, name: "West Seattle Golf Course" },
    { id: 7, name: "Snoqualmie Falls" },
    { id: 8, name: "Allenmore Golf Course" },
    { id: 9, name: "Redmond Ridge" },
    { id: 10, name: "Mount Si Golf Course" },
    { id: 11, name: "Riverbend Golf Course" },
    { id: 12, name: "Bellevue Golf Course" },
    { id: 13, name: "Apple Tree Golf Course" },
    { id: 14, name: "Druid Glenn Golf Club" },
    { id: 15, name: "The Links at Moses Pointe" }
  ];
  constructor(
    private router: Router,
    private scoreService: ScoreService
  ) {}

  mapCourseName(courseId: number): string {
    const courseMap: Record<number, string> = {
      1: 'Coyote Creek',
      2: 'Eagles Talon',
      3: 'Maplewood Golf Course',
      4: 'Fosters Golf Links',
      5: 'Auburn Golf Course',
      6: 'West Seattle Golf Course',
      7: 'Snoqualmie Falls',
      8: 'Allenmore Golf Course',
      9: 'Redmond Ridge',
      10: 'Mount Si Golf Course',
      11: 'Riverbend Golf Course',
      12: 'Bellevue Golf Course',
      13: 'Apple Tree Golf Course',
      14: 'Druid Glenn Golf Club',
      15: 'The Links at Moses Pointe',
    };
    const courseName = courseMap[courseId] || 'Unknown Course';
    console.log(`Course ID ${courseId} maps to Course Name: ${courseName}`);
    return courseName;
  }
  onSubmit() {
    this.scoreService.addScore(this.score).subscribe(
      (response) => {
        console.log('Score added successfully', response);
        this.router.navigate(['/home']);
      },
      (error) => {
        console.error('Error adding score', error);
      }
    );
  }
}
