import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScoreService } from '../services/score.service';
import { AuthenticationService } from '../services/authentication.service';


@Component({
  selector: 'app-course-info',
  templateUrl: './course-info.component.html',
  styleUrls: ['./course-info.component.css']
})
export class CourseInfoComponent implements OnInit {
  selectedCourse: any = {
    courseName: '',
    location: '',
    par: 0
  };
  scores: any[] = [];

  constructor(
    private route: ActivatedRoute,
    private scoreService: ScoreService,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const courseId = +params['courseId'];
      this.loadCourseInfo(courseId);
      this.loadScores(courseId);
    });
  }

  loadCourseInfo(courseId: number) {
    switch (courseId) {
      case 1:
        this.selectedCourse = {
          courseName: 'Coyote Creek',
          location: 'Redmond, Wa',
          par: 72,
          imageUrl: 'https://exddilid.cdn.imgeng.in/app/courses/image/preview/82675.jpg'
        };
        break;
      case 2:
        this.selectedCourse = {
          courseName: 'Eagles Talon',
          location: 'Redmond, Wa',
          par: 71,
          imageUrl: 'https://exddilid.cdn.imgeng.in/app/courses/image/preview/99117.jpg'
        };
        break;
      case 3:
        this.selectedCourse = {
          courseName: 'Maplewood Golf Course',
          location: 'Renton, Wa',
          par: 71,
          imageUrl: 'https://assets.hole19golf.com/course_images/images/000/053/202/standard/2c6c3df6ecbdb3d4027fd22d3b3d4e32a59be411.jpeg?1552561777'
        };
        break;
      case 4:
        this.selectedCourse = {
          courseName: 'Fosters Golf Links',
          location: 'Tukwila, Wa',
          par: 68,
          imageUrl: 'https://golf-pass.brightspotcdn.com/dims4/default/b49c652/2147483647/strip/true/crop/1440x929+0+15/resize/930x600!/format/webp/quality/90/?url=https%3A%2F%2Fgolf-pass-brightspot.s3.amazonaws.com%2Fd4%2Fdc%2F366b8c0e2f6257d78c5ced3181e3%2F98849.jpg'
        };
        break;
      case 5:
        this.selectedCourse = {
          courseName: 'Auburn Golf Course',
          location: 'Auburn, Wa',
          par: 71,
          imageUrl: 'https://www.auburngolf.org/wp-content/uploads/sites/4809/2022/02/image_1_1940x640.png'
        };
        break;
      case 6:
        this.selectedCourse = {
          courseName: 'West Seattle Golf Course',
          location: 'Seattle, Wa',
          par: 72,
          imageUrl: 'https://dynamic-media-cdn.tripadvisor.com/media/photo-o/06/28/4f/65/west-seattle-golf-club.jpg?w=2000&h=-1&s=1'
        };
        break;
      case 7:
        this.selectedCourse = {
          courseName: 'Snoqualmie Falls',
          location: 'Fall City, Wa',
          par: 71,
          imageUrl: 'https://scontent-sea1-1.xx.fbcdn.net/v/t39.30808-6/302184563_505769651551030_7257166233255863365_n.jpg?stp=dst-jpg_p180x540&_nc_cat=102&ccb=1-7&_nc_sid=5f2048&_nc_ohc=Jhl499-mGNYAX9e0Z0K&_nc_ht=scontent-sea1-1.xx&oh=00_AfBIefCo1ydOpa45xuj7XGKjRywcslcj6CLeJhX6gf0J9w&oe=652C8650'
        };
        break;
      case 8:
        this.selectedCourse = {
          courseName: 'Allenmore Golf Course',
          location: 'Tacoma, Wa',
          par: 71,
          imageUrl: 'https://assets.simpleviewinc.com/simpleview/image/fetch/c_fill,h_672,q_75,w_1024/http://res.cloudinary.com/simpleview/image/upload/v1518114292/clients/tacoma/Founatin__d4a1eb8c-256b-4445-bbdd-a4738c2a9c4a.jpg'
        };
        break;
      case 9:
        this.selectedCourse = {
          courseName: 'Redmond Ridge',
          location: 'Redmond, Wa',
          par: 72,
          imageUrl: 'https://www.womensgolfday.com/wp-content/uploads/2021/01/RRHole_18.jpg'
        };
        break;
      case 10:
        this.selectedCourse = {
          courseName: 'Mount Si Golf Course',
          location: 'Snoqualmie, WA',
          par: 72,
          imageUrl: 'https://www.mtsigolf.com/wp-content/uploads/sites/7419/2019/04/IMG_7005-1.jpeg?resize=1000,640'
        };
        break;
      case 11:
        this.selectedCourse = {
          courseName: 'Riverbend Golf Course',
          location: 'Kent, Wa',
          par: 72,
          imageUrl: 'https://exddilid.cdn.imgeng.in/app/courses/image/preview/61109.jpg'
        };
        break;
      case 12:
        this.selectedCourse = {
          courseName: 'Bellevue Golf Course',
          location: 'Bellevue, Wa',
          par: 71,
          imageUrl: 'https://cdn.cybergolf.com/images/2279/Bellevue_50-H14_1920x1280.jpg'
        };
        break;
      case 13:
        this.selectedCourse = {
          courseName: 'Apple Tree Golf Course',
          location: 'Yakima, Wa',
          par: 72,
          imageUrl: 'https://www.visityakima.com/img/image.php?id=2924'
        };
        break;
      case 14:
        this.selectedCourse = {
          courseName: 'Druid Glenn Golf Club',
          location: 'Covington, Wa',
          par: 72,
          imageUrl: 'https://thumb.spokesman.com/Q5lhNuCN_XJdJipaX5tHPb_pkBU=/1200x800/smart/media.spokesman.com/photos/2012/08/10/glf_10_druids_No._2.jpg'
        };
        break;
      case 15:
        this.selectedCourse = {
          courseName: 'The Links at Moses Pointe',
          location: 'Moses Lake, Wa',
          par: 72,
          imageUrl: 'https://cdn.cybergolf.com/images/1843/slidepic1.jpg'
        };
        break;
      default:
        this.selectedCourse = {
          courseName: 'Unknown Course',
          location: 'Unknown Location',
          par: 0,
          imageUrl: 'unkown-course-image.jpg'
        };
        break;
    }
  }


  loadScores(courseId: number) {
    const userEmail = this.authService.getUserEmail();

    if (!userEmail) {
      console.error('User email not available.');
      return;
    }

    this.scoreService.getUserScoresByCourse(userEmail, courseId).subscribe(
      (scores) => {
        this.scores = scores;
      },
      (error) => {
        console.error('Error loading scores', error);
      }
    );
  }
  deleteScore(scoreId: number) {
    this.scoreService.deleteScore(scoreId).subscribe(
      () => {
        this.scores = this.scores.filter((score) => score.id !== scoreId);
      },
      (error) => {
        console.error('Error deleting score', error);
      }
    );
  }
  editScore(scoreId: number) {
    this.router.navigate(['/edit', scoreId]);
  }
  goToScorePost() {
    this.router.navigate(['/addscore']);
  }
  signOut() {
    this.authService.signOut();
    this.router.navigate(['/login']);
  }
  goToHome() {
    this.router.navigate(['/home']);
  }
}
