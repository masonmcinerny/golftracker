import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})


export class ScoreService {
  private baseUrl = 'http://localhost:5111/api/scores/';

  constructor(private http: HttpClient) {}

  getUserScoresByEmail(userEmail: string): Observable<any[]> {
    const url = this.baseUrl + 'user/' + userEmail;
    return this.http.get<any[]>(url);
  }

  getUserScoresByCourse(userEmail: string, courseId: number): Observable<any[]> {
    const url = this.baseUrl + userEmail + '/' + courseId; 
    return this.http.get<any[]>(url);
  }
  addScore(score: any): Observable<any> {
    const url = this.baseUrl;
    return this.http.post<any>(url, score);
  }
  deleteScore(scoreId: number): Observable<void> {
    const url = `${this.baseUrl}${scoreId}`;
    return this.http.delete<void>(url);
  }

  editScore(score: any): Observable<any> {
    const url = `${this.baseUrl}${score.id}`;
    return this.http.put(url, score);
  }
  getScoreById(scoreId: number): Observable<any> {
    const url = `${this.baseUrl}${scoreId}`;
    return this.http.get<any>(url);
  }

}
