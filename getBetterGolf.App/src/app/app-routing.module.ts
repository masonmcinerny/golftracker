import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthGuard } from './auth/guards/auth.guard';
import { ScorePostComponent } from './score-post/score-post.component';
import { CourseInfoComponent } from './course-info/course-info.component';
import { EditScoreComponent } from './edit-score/edit-score.component';
import { LoginAlternativeComponent } from './auth/login-alternative/login-alternative.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'auth/login-alternative', component: LoginAlternativeComponent },
  { path: 'auth/register', component: RegisterComponent },
  { path: 'addscore', component: ScorePostComponent },
  { path: 'course/:courseId', component: CourseInfoComponent },
  { path: 'edit/:scoreId', component: EditScoreComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
