# GetBetterGolF
    A golf app designed in order to advance score recording. Compare scores on courses with previously entered scored.


## Getting started
    After cloning repo:

    -Backend:

        Change directory into the GolfTracker directory and enter the command: Dotnet run.

    -Frontend:

        change directory into the GetBetterGolf.App directory enter the command: Ng Serve.

    This allows both apps to be working in order to use the app.

## Backend

## Models
    - AuthResult

        Purpose:
            This model is used for authentication results and tokens.
        Properties:
            Token: A string that represents an authentication token.
        Result:
            A boolean value indicating the result of the authentication.
        Errors:
            A list of strings that contains any authentication errors (nullable).

    - LoginRequestResponse

        Purpose:
            This model extends AuthResult and is used for login request responses.
        Properties:
            Inherits the properties from AuthResult.

    - RegistrationRequestResponse

        Purpose:
            This model extends AuthResult and is used for registration request responses.
        Properties:
            Inherits the properties from AuthResult.

    - UserLoginRequestDto

        Purpose:
            This model is used for user login requests.
        Properties:
            Email: A required string field representing the user's email.
            Password: A required string field representing the user's password.

- UserRegistrationRequestDto

        Purpose:
            This model is used for user registration requests.
        Properties:
            Name: A required string field representing the user's name.
            Email: A required string field representing the user's email.
        Password: A required string field representing the user's password.

    - GolfCourse

        Purpose:
            This model represents golf course details.
        Properties:
            Id: An auto-generated unique identifier for the golf course.
            CourseName: The name of the golf course.
            Location: The location of the golf course.
            Par: The par value of the golf course.

    - Score

        Purpose: This model represents golf score details.
        Properties:
            Id: An auto-generated unique identifier for the score.
            UserName: The name of the user associated with the score.
            GolfCourseId: The ID of the golf course associated with the score.
            EntryDate: The date of the score entry.
            Tees: The type of tees used for the game.
            StartingPosition: The starting position on the course.
            Holes: The number of holes played.
            Putts: The number of putts taken.
            TotalScore: The total score achieved.
            Notes: Additional notes or comments about the score.

## Database
    - Sqlite was used in order to not require set up of a separate database server. Due to the factor that new golfcourses
        are not being built everyday the only real stat tracking was scores posted on golf courses.


## Api Request:


## AuthManagementController

The `AuthManagementController` is responsible for user authentication, including registration and login. It leverages Microsoft's ASP.NET Core Identity for user management and token generation.

### Dependencies

- `ILogger<AuthManagementController>`: Provides logging functionality for the controller.
- `UserManager<IdentityUser>`: Manages user-related operations using ASP.NET Identity.
- `JwtConfig`: Configuration options for JWT (JSON Web Token) generation.
- `IOptionsMonitor<JwtConfig>`: Provides access to JWT configuration settings.

### Methods

#### Register
- **POST** `/api/AuthManagement/Register`
- Accepts a `UserRegistrationRequestDto` containing user registration details.
- Validates the request and checks if the user with the given email already exists.
- If not, creates a new user using ASP.NET Identity, generates a JWT token, and returns it in the response.
- If there are errors during user creation, it returns a 400 Bad Request response with error details.

#### Login
- **POST** `/api/AuthManagement/Login`
- Accepts a `UserLoginRequestDto` containing user login credentials.
- Validates the request, checks if the user exists, and verifies the password.
- If the credentials are valid, generates a JWT token and returns it in the response.
- If the user does not exist or the password is incorrect, it returns a 400 Bad Request response.

#### GenerateJwtToken
- A private method for generating a JWT token.
- It sets the token's subject and claims, defines its expiration time, and signs it with a secret key from the JWT configuration.

The controller uses JWT for secure authentication, ensuring users can register, log in, and access protected resources while maintaining security and authorization.

## ScoreController

The `ScoreController` is responsible for managing golf scores, including retrieval, creation, updating, and deletion. It's protected with the [Authorize] attribute, ensuring that only authenticated users can access its endpoints.

### Dependencies
- `ApiDbContext`: Represents the database context for your application, allowing interaction with the database.
- `UserManager`<IdentityUser>: Provides user management functionality using ASP.NET Identity.


### Methods

#### GetScores
- **GET** `/api/scores`
- Retrieves a list of golf scores associated with the currently authenticated user.
- The user's scores are filtered based on their username.
- Returns a collection of scores.

#### GetScore
- GET `/api/scores/{id}`
Retrieves a specific golf score by its ID.
Returns the score if found, or a "Not Found" response if the score doesn't exist.

#### PostScore
- **POST** `/api/scores`
- Creates a new golf score.
- Checks if the specified golf course exists.
- Obtains the user's email address from claims and associates it with the score.
- Adds the new score to the database and returns a "Created" response with the created score.

      JSON body {
                    "id": 0,
                    "userName": "string",
                    "golfCourseId": 0,
                    "entryDate": "2023-10-12",
                    "tees": "string",
                    "startingPosition": 0,
                    "holes": 0,
                    "putts": 0,
                    "totalScore": 0,
                    "notes": "string"
                }

#### PutScore
- **PUT** `/api/scores/{id}`
- Updates an existing golf score based on its ID.
- Validates that the provided score ID matches the ID in the request.
- Marks the score as modified in the database context and saves the changes.
- Returns a "No Content" response if the update is successful.

#### DeleteScore
- **DELETE** `/api/scores/{id}`
- Deletes a golf score based on its ID.
- Retrieves the score by its ID and removes it from the database.
- Returns a "No Content" response after successful deletion.

#### GetScoresByCourse
- **GET**  `/api/scores/{userName}/{courseId}`
- Retrieves golf scores by filtering for a specific user and golf course.
- Returns a collection of scores associated with the provided username and golf course ID.

#### GetScoresByUser
- **GET** `/api/scores/user/{userName}`
- Retrieves golf scores by filtering for a specific user.
- Uses the UserManager to retrieve user information, ensuring the user exists.
- Returns a collection of scores associated with the provided username.


This controller allows authenticated users to manage their golf scores, including adding, updating, and deleting them, as well as retrieving scores by user or course. It provides secure access to score-related resources while maintaining proper authorization and authentication.

## ApiDbContext
- The ApiDbContext class is responsible for managing the database context for the application.
   It inherits from IdentityDbContext, which is part of ASP.NET Identity and is used for user management.

### Dependencies
- DbContextOptions<ApiDbContext>: Provides options for configuring the database context.

### DbSet
- GolfCourses: Represents a database table for storing golf course information.
- Scores: Represents a database table for storing golf scores.

### Data Seeding
- Inside the OnModelCreating method, you'll notice that there is a section where golf course data is being seeded into the database.
    This hardcoded data includes information about various golf courses, such as their names, locations, and par values.
- This approach is taken because, in this application, only administrators have the privilege to add new golf courses.
   Regular users will interact with existing courses to post   their scores. By seeding the initial data, you ensure that a set of predefined golf courses is available for users to choose from when posting scores.
- Since user-generated courses are not allowed, this static data provides a convenient way to populate the application with known golf courses
    for score tracking. It simplifies the user experience and maintains consistency in the available courses.

# Testing Backend
To test Auth navigate use swagger at: `http://localhost:5111/swagger/index.html`
Register a user and copy the JWT token that is generated.

To test score capabilites.
Using insomnia or similar service add JWT Token (from swagger) into the bearer aspect.

## FrontEnd

### Features
The frontend of the Golf Score Tracking Web App includes the following features:

### User Authentication: Users can log in or register for an account using their email and password.
Score Tracking: Registered users can record their golf scores, including details like the golf course played, tees used, starting position, holes played, putts, and notes.
Course Information: Users can view detailed information about different golf courses, including the course name, location, and par value.
User Dashboard: Authenticated users can see a list of their golf scores, filter by golf course, edit, and delete scores.
Responsive Design: The application is designed to work on various devices, including desktops, tablets, and mobile phones.

### Getting Started

Open a terminal in the project directory and run npm install to install the project's dependencies.
Start the Angular development server with ng serve.
Open a web browser and navigate to `http://localhost:4200/` to access the application.

### Application Structure
The frontend is structured as follows:

### Components:
The application is organized into different components, such as LoginComponent, RegisterComponent, HomeComponent, ScorePostComponent, CourseInfoComponent, and EditScoreComponent, among others.

- Guards: The AuthGuard is used to protect routes that require authentication. Users are redirected to the login page if not authenticated.

- Services: The AuthenticationService handles user authentication, and the ScoreService manages score-related operations.

- Interceptors: The AuthenticationInterceptor adds the JWT token to the HTTP request headers for authenticated users.

- Authentication
    The AuthenticationService manages user authentication. Users can log in using their email and password or register for a new account. After successful authentication, a JSON Web Token (JWT) is stored in the browser's local storage.

- Score Management
The ScorePostComponent allows users to post their golf scores, including information about the golf course played, tees used, starting position, holes played, putts, and notes. Users can also edit and delete their scores.

- Golf Course Information
The CourseInfoComponent displays detailed information about various golf courses. Users can view information about the course's name, location, par value, and an image of the course. Course data is hardcoded and available for users to select when posting scores. It also displays the users posted scores on that course

- Navigation
Navigation between different parts of the application is achieved through the Angular Router. Users can navigate between the login page, registration page, home/dashboard, score posting, golf course details, and score editing.

- API Integration
The frontend interacts with a backend API to perform operations such as user authentication, score posting, editing, and retrieval of golf course information.

